<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%
    String title = "Accessing Request Param";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'EL.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	
  </head>
  
  <body>
  <jsp:useBean id="box" 
                    class="com.tutorialspoint.box">
   		<jsp:setProperty name="box" property="perimeter" value="100"/>
   		<jsp:setProperty name="box" property="width" value="100"/>
   		<jsp:setProperty name="box" property="height" value="100"/>
   		${expr}
   		<jsp:setProperty name="box" property="perimeter" 
                 value="${2*box.width+2*box.height}"/>
   </jsp:useBean>
                 <jsp:text>
Box Perimeter is: ${2*box.width + 2*box.height}
</jsp:text>
<center>
<h1><% out.print(title); %></h1>
</center>
<div align="center">
<!-- request.getParameter和request.getParameterValues方法可用的参数值。 -->
<p>${param["username"]}</p>
${box.height}
${pageContext.request.queryString}
</div>
  </body>
</html>
